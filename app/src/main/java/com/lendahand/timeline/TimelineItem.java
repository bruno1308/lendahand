package com.lendahand.timeline;

/**
 * Created by Bruno on 12/12/2015.
 */
public class TimelineItem {
    String user, time_ago, post_text;
    int user_photo_id;

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    boolean liked = false;

    public int getPost_pic_id() {
        return post_pic_id;
    }

    public void setPost_pic_id(int post_pic_id) {
        this.post_pic_id = post_pic_id;
    }

    public String getPost_text() {
        return post_text;
    }

    public void setPost_text(String post_text) {
        this.post_text = post_text;
    }

    public int getUser_photo_id() {
        return user_photo_id;
    }

    public void setUser_photo_id(int user_photo_id) {
        this.user_photo_id = user_photo_id;
    }

    public String getTime_ago() {
        return time_ago;
    }

    public void setTime_ago(String time_ago) {
        this.time_ago = time_ago;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    int post_pic_id;

}

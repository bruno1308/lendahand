package com.lendahand.timeline;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.lendahand.R;

import java.util.ArrayList;

public class TimelineFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static Context context;
    public static ArrayList<TimelineItem> posts;
    public TimelineFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static TimelineFragment newInstance(Context c) {
        TimelineFragment fragment = new TimelineFragment();
        context = c;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_timeline, container, false);
        ListView timeline = (ListView) rootView.findViewById(R.id.listview_timeline);

        TimelineItemAdapter timeline_items = new TimelineItemAdapter(getContext(),R.id.listview_timeline,getPosts(context));
        timeline.setAdapter(timeline_items);

        return rootView;
    }



    public ArrayList<TimelineItem> getPosts(Context c){
        posts = new ArrayList<>();
        TimelineItem t1 = new TimelineItem();
        t1.setUser("Bruno Fernandes");
        t1.setPost_text("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.");
        t1.setTime_ago(" - 5 min ago");
        t1.setUser_photo_id(R.mipmap.fb);

        int resID = c.getResources().getIdentifier("family" , "drawable", c.getPackageName());
        t1.setPost_pic_id(resID);




        TimelineItem t2 = new TimelineItem();
        t2.setUser("Fúlvio Abrahão");
        t2.setPost_text("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.");
        t2.setTime_ago(" - 10 min ago");
        t2.setUser_photo_id(R.drawable.fb_fulvio);
        int resID2 = c.getResources().getIdentifier("family2" , "drawable", c.getPackageName());
        t2.setPost_pic_id(resID2);


        posts.add(t1);
        posts.add(t2);
        return posts;
    }
}

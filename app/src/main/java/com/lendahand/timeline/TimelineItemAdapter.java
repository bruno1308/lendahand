package com.lendahand.timeline;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lendahand.R;

import java.util.ArrayList;

/**
 * Created by Bruno on 12/12/2015.
 */

public class TimelineItemAdapter extends ArrayAdapter<TimelineItem> {

    Context context;
    int layoutResourceId;
    ArrayList<TimelineItem> data = null;

    public TimelineItemAdapter(Context context, int layoutResourceId, ArrayList<TimelineItem> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PostHolder holder = null;
        final TimelineItem item_being_rendered = data.get(position);

        if (row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(R.layout.timeline_single_item, parent, false); //Trocar por listview_item para visao com lista

            holder = new PostHolder();
            holder.user_name = (TextView)row.findViewById(R.id.txtUsername);
            holder.user_img = (ImageView)row.findViewById(R.id.imgUserPic);
            holder.post_img = (ImageView)row.findViewById(R.id.imgPostPic);
            holder.post_text = (TextView)row.findViewById(R.id.txtPostText);
            holder.time_ago = (TextView) row.findViewById(R.id.txtTimeAgo);
            holder.like = (ImageView)row.findViewById(R.id.imgLike);
            holder.comment = (ImageView)row.findViewById(R.id.imgComment);
            holder.favorite = (ImageView)row.findViewById(R.id.imgFavr);



            row.setTag(holder);


        }
        else {
            holder = (PostHolder)row.getTag();
        }
        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item_being_rendered.setLiked(!item_being_rendered.isLiked());
                ImageView imageView = (ImageView) v;
                imageView.setImageResource(item_being_rendered.isLiked()? R.mipmap.like_filled : R.mipmap.like_light);
            }
        });



        //Save values into class
        holder.user_name.setText(item_being_rendered.getUser());
        holder.user_img.setImageResource(item_being_rendered.getUser_photo_id());
        holder.post_img.setImageResource(item_being_rendered.getPost_pic_id());
        holder.post_text.setText(item_being_rendered.getPost_text());
        holder.time_ago.setText(item_being_rendered.getTime_ago());

        holder.like.setImageResource(item_being_rendered.isLiked()? R.mipmap.like_filled : R.mipmap.like_light);

        return row;
    }

    static class PostHolder
    {
        public ImageView user_img;
        public TextView user_name;
        public ImageView post_img;
        public TextView post_text;
        public TextView time_ago;
        public ImageView like;
        public ImageView comment;
        public ImageView favorite;

    }





}
package com.lendahand;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NewFamilyActivity extends AppCompatActivity {
    private Uri outputFileUri;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-kk-mm-ss");
    private static final int YOUR_SELECT_PICTURE_REQUEST_CODE = 13;
    private  static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 7;

    EditText familyName, familyNumberMembers, familyCountry, familyCity, familyAmountNeeded,familyTextPublish;
    ImageView familyPictureTaken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_family);
        setupToolbar();
        loadUI();
    }

    private void openImageIntentWhatTheFuck(View view) {

// Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator+sdf.format(new Date()));
        boolean createdDir = root.mkdirs();
        final String fname = "myfile_";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        startActivityForResult(chooserIntent, YOUR_SELECT_PICTURE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == YOUR_SELECT_PICTURE_REQUEST_CODE) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String dataString = data.getDataString();
                    if (dataString == null || dataString.length()<2) {
                        isCamera = true;
                    } else {
                        isCamera = false;
                    }
                }

                Uri selectedImageUri;
                familyPictureTaken=(ImageView)findViewById(R.id.imgPictureTaken);
                if (isCamera) {
                    selectedImageUri = outputFileUri;

                } else {
                    selectedImageUri = data.getData();
                }

                familyPictureTaken.setImageURI(selectedImageUri);


               /* imgView.setAdjustViewBounds(true);
                imgView.setScaleType(ImageView.ScaleType.FIT_START);
               // imgView.setImageDrawable(null);
                imgView.setImageURI(selectedImageUri);
                imgView.invalidate();*/


            }
        }
    }

    public void openImageIntent(View view) {
        if (ContextCompat.checkSelfPermission(NewFamilyActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || (ContextCompat.checkSelfPermission(NewFamilyActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(NewFamilyActivity.this,
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(NewFamilyActivity.this,
                        new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else {

            openImageIntentWhatTheFuck(view);
        }

    }

    public void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarNewFamily);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.menu_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Add new family");
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    openImageIntentWhatTheFuck(null);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void handleSendNewFamily(View view) {
        //TODO
        //Get strings and numbers from UI and send to server
    }

    public void loadUI(){
        familyName = (EditText) findViewById(R.id.txtFamilyName);
        familyNumberMembers = (EditText) findViewById(R.id.txtFamilyNumberMembers);
        familyCountry = (EditText) findViewById(R.id.txtFamilyCountry);
        familyCity = (EditText) findViewById(R.id.txtFamilyCity);
        familyAmountNeeded = (EditText) findViewById(R.id.txtFamilyAmountNeeded);
        familyTextPublish = (EditText) findViewById(R.id.txtTextToPublish);
        familyPictureTaken=(ImageView)findViewById(R.id.imgPictureTaken);
    }
}

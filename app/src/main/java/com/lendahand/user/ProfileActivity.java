package com.lendahand.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;

import com.lendahand.GodfatherActivity;
import com.lendahand.R;
import com.lendahand.AngelActivity;
import com.lendahand.timeline.TimelineFragment;
import com.lendahand.timeline.TimelineItemAdapter;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });

        TimelineFragment timelineFragment = new TimelineFragment();
        ListView timeline = (ListView) findViewById(R.id.listview_profile);
        timeline.addHeaderView(LayoutInflater.from(this).inflate(R.layout.header_profile, null));
        TimelineItemAdapter timeline_items = new TimelineItemAdapter(ProfileActivity.this,R.id.listview_timeline,timelineFragment.getPosts(this));
        timeline.setAdapter(timeline_items);
       // timeline.setFocusable(false);

        timeline.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        setupToolbar();

    }

    public void btnFollow(View view) {
        ImageButton imgFollow = (ImageButton)(findViewById(R.id.btnFollow));
        imgFollow.setImageResource(R.drawable.following);
    }

    public void handleProfileClick(View view) {
        Intent i = new Intent(ProfileActivity.this, ProfileActivity.class);
        startActivity(i);
        finish();
    }


    public void rankingAngel(View view) {
        Intent i = new Intent(ProfileActivity.this, AngelActivity.class);
        startActivity(i);
    }

    public void rankingGodfather(View view) {
        Intent i = new Intent(ProfileActivity.this, GodfatherActivity.class);
        startActivity(i);
    }

    public void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_profile);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.menu_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Profile");
    }
}

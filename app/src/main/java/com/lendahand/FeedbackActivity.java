package com.lendahand;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.VideoView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FeedbackActivity extends AppCompatActivity {
    private Uri outputFileUri;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-kk-mm-ss");
    private static final int YOUR_SELECT_PICTURE_REQUEST_CODE = 13;
    private  static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 7;
    private  static final int REQUEST_VIDEO_CAPTURE = 1;

    ImageView familyPictureTaken,familyVideoTaken;
    VideoView mVideoView;

    AutoCompleteTextView textView;
    private int stopPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if( savedInstanceState != null ) {
            stopPosition = savedInstanceState.getInt("position");
        }
        setContentView(R.layout.activity_feedback);
        loadUI();


        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                FeedbackActivity.this, android.R.layout.simple_dropdown_item_1line,
                new String[]{"Família A","Família Souza","Família Fernandes","Família Lopes"});



        textView.setAdapter(arrayAdapter);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                textView.showDropDown();
            }
        });
    }

    public void handleRecordVideo(View view) {
      checkVideoPermission();
    }

    public void loadUI(){
        textView = (AutoCompleteTextView) findViewById(R.id.txtFeedbackFamilyName);
        familyVideoTaken =(ImageView)findViewById(R.id.imgVideoTaken);
        familyPictureTaken=(ImageView)findViewById(R.id.imgFeedbackPictureTaken);
        mVideoView = (VideoView)findViewById(R.id.vidVideoTaken);
    }


    private void openImageIntentWhatTheFuck(View view) {

// Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "Feedback" + File.separator+sdf.format(new Date()));
        boolean createdDir = root.mkdirs();
        final String fname = "feedback";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        startActivityForResult(chooserIntent, YOUR_SELECT_PICTURE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == YOUR_SELECT_PICTURE_REQUEST_CODE) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String dataString = data.getDataString();
                    if (dataString == null || dataString.length()<2) {
                        isCamera = true;
                    } else {
                        isCamera = false;
                    }
                }

                Uri selectedImageUri;
                if (isCamera) {
                    selectedImageUri = outputFileUri;

                } else {
                    selectedImageUri = data.getData();
                }

                familyPictureTaken.setImageURI(selectedImageUri);



            }else if(requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK){
                Uri videoUri = data.getData();
                familyVideoTaken.setVisibility(View.GONE);
                mVideoView = (VideoView)findViewById(R.id.vidVideoTaken);
                mVideoView.setClickable(true);
                mVideoView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        handlePlayVideo(view);
                        return false;
                    }
                });
                mVideoView.setVisibility(View.VISIBLE);
                mVideoView.setVideoURI(videoUri);
            }
        }
    }

    public void openImageIntent(View view) {
        if (ContextCompat.checkSelfPermission(FeedbackActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || (ContextCompat.checkSelfPermission(FeedbackActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(FeedbackActivity.this,
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(FeedbackActivity.this,
                        new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else {

            openImageIntentWhatTheFuck(view);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    openImageIntentWhatTheFuck(null);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void handlePlayVideo(View view) {
        if(mVideoView.isPlaying()){
            mVideoView.pause();
        }
        else{
            mVideoView.start();
        }

    }

    public void checkVideoPermission(){
        if (ContextCompat.checkSelfPermission(FeedbackActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || (ContextCompat.checkSelfPermission(FeedbackActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) || ((ContextCompat.checkSelfPermission(FeedbackActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED))) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(FeedbackActivity.this,
                    Manifest.permission.CAMERA)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(FeedbackActivity.this,
                        new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else {

            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
       // Log.d(TAG, "onResume called");
        mVideoView.seekTo(stopPosition);
        mVideoView.start(); //Or use resume() if it doesn't work. I'm not sure
    }

    // This gets called before onPause so pause video here.
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        stopPosition = mVideoView.getCurrentPosition();
        mVideoView.pause();
        outState.putInt("position", stopPosition);
    }

    public void handleSendFeedbacky(View view) {
        //TODO
        //Send feedback to server
    }
}

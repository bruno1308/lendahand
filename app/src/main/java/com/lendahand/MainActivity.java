package com.lendahand;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.amazonaws.mobileconnectors.cognito.Dataset;
import com.amazonaws.mobileconnectors.cognito.DefaultSyncCallback;
import com.facebook.login.LoginManager;
import com.lendahand.chat.InboxFragment;
import com.lendahand.family.FamilyFragment;
import com.lendahand.timeline.TimelineFragment;
import com.lendahand.user.ProfileActivity;

import java.util.List;


public class MainActivity extends AppCompatActivity {
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private final int TIMELINE = 0;
    private final int MESSAGES = 1;
    private final int FAMILIES = 2;


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    TabLayout tabLayout;

    private void setupTabIcons() {
        tabLayout.getTabAt(FAMILIES).setIcon(R.mipmap.family);
        tabLayout.getTabAt(MESSAGES).setIcon(R.mipmap.message_darkgray);
        tabLayout.getTabAt(TIMELINE).setIcon(R.mipmap.globe);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Log.i("Lifecycle", "OnCreate Called");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
      //  toolbar.inflateMenu(R.menu.menu_toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                changeTabColors(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        setupTabIcons();
        tabLayout.getTabAt(TIMELINE).setIcon(R.mipmap.globe_blue);



    }

    private void changeTabColors(int position) {
        setupTabIcons();
        if (position == TIMELINE) {
            tabLayout.getTabAt(position).setIcon(R.mipmap.globe_blue);
        }else if(position == MESSAGES){
            tabLayout.getTabAt(position).setIcon(R.mipmap.message_blue);
        }else{
            tabLayout.getTabAt(position).setIcon(R.mipmap.family_blue);
        }



    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("Lifecycle", "OnStart Called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("Lifecycle", "OnPause Called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Lifecycle", "OnDestroy Called");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == R.id.action_logoff){
            LoginManager.getInstance().logOut();
            Intent i = new Intent(MainActivity.this,LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }


    public void handleProfileClick(View view) {
        Intent i = new Intent(MainActivity.this, ProfileActivity.class);
        startActivity(i);
    }

    public void handleFamilyClick(View view){
        Intent i = new Intent(MainActivity.this, CheckoutActivity.class);
        startActivity(i);
    }

    public void handleNewFamilyClick(View view) {
        Intent i = new Intent(MainActivity.this, NewFamilyActivity.class);
        startActivity(i);
    }

    public void handleProvideFeeback(View view) {
        Intent i = new Intent(MainActivity.this, FeedbackActivity.class);
        startActivity(i);
    }

    /**
     * A placeholder fragment containing a simple view.
     */


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {



        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            if(position == TIMELINE){

                return TimelineFragment.newInstance(MainActivity.this);
            }else if(position ==MESSAGES ){
               return  InboxFragment.newInstance(MainActivity.this);

            }else if(position == FAMILIES){
               return FamilyFragment.newInstance(MainActivity.this);
            }

            //Should never reach here
            return  null;

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
           /* switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }*/
            return null;
        }




    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Lifecycle","OnResume Called");

    }


}

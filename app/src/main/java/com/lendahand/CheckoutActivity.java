package com.lendahand;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

public class CheckoutActivity extends AppCompatActivity {

    private SeekBar seekBarFamily;
    private SeekBar seekBarMaint;
    private Button btn50;
    private Button btn10;
    private Button checkout;
    private final int familyPercent = 85;
    private final int devPercent = 15;
    private double donationValue = 10.0;
    private EditText txtCustomValue;
    private TextView seekFamilyValue;
    private TextView seekDevValue;
    private TextView agreement;


    //Username: lopesfernande_api1.hotmail.com
    //Password: 7CJT8V663HQNWCE9
    //Signature: AHLWP3KJHBhj6D049lNtMZAIU4dMAkcluErwOvAL6tBxw1wH0ULkEFKR

    //App ID: APP-6Y6710707H299641E
    private static PayPalConfiguration config = new PayPalConfiguration()

            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
            .languageOrLocale("pt_BR")
            .clientId("AHLWP3KJHBhj6D049lNtMZAIU4dMAkcluErwOvAL6tBxw1wH0ULkEFKR");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        Intent intent = new Intent(this, PayPalService.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        startService(intent);
        initUI();

    }

    private void setupSeekbar(double donationValue) {
        seekBarFamily.setProgress(familyPercent);
        seekBarMaint.setProgress(devPercent);
        correctText(familyPercent, devPercent);

    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    public void confirmPayment(View pressed) {

        // PAYMENT_INTENT_SALE will cause the payment to complete immediately.
        // Change PAYMENT_INTENT_SALE to
        //   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
        //   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
        //     later via calls from your server.

        PayPalPayment payment = new PayPalPayment(new BigDecimal(donationValue), "USD", "Item Teste",
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, 0);


    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i("paymentExample", confirm.toJSONObject().toString(4));

                    // TODO: send 'confirm' to your server for verification.
                    // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                    // for more details.
                    Toast.makeText(CheckoutActivity.this,"Your payment was approved, thank you very much!",Toast.LENGTH_LONG);
                    finish();

                } catch (JSONException e) {
                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                }
            }
        }
        else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        }
        else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
    }

    public void btn10(View v){
        donationValue = 10.0;
        setupSeekbar(donationValue);

    }
    public void btn50(View v){
        donationValue = 50.0;
        setupSeekbar(donationValue);
    }


    private void initUI(){
        seekBarFamily = (SeekBar)findViewById(R.id.skbCharity);
        seekBarMaint = (SeekBar)findViewById(R.id.skbDev);
        txtCustomValue = (EditText)findViewById(R.id.txtCustomValue);
        seekDevValue = (TextView) findViewById(R.id.txtSeekMaintVal);
        seekFamilyValue = (TextView) findViewById(R.id.txtSeekFamilyVal);
        checkout = (Button) findViewById(R.id.btnCheckout);
        agreement = (TextView)findViewById(R.id.txtAgreement);
        agreement.setText(Html.fromHtml(getString(R.string.checkout_text)));

        txtCustomValue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_NULL
                        && event.getAction() == KeyEvent.ACTION_DOWN) {
                    donationValue = Double.parseDouble(txtCustomValue.getText().toString());
                    setupSeekbar(donationValue);

                }
                return true;

            }
        });
        txtCustomValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()!=0) {
                    donationValue = Double.parseDouble(txtCustomValue.getText().toString());
                    setupSeekbar(donationValue);
                }

            }
        });

        txtCustomValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if(!txtCustomValue.getText().toString().isEmpty()) {
                        donationValue = Double.parseDouble(txtCustomValue.getText().toString());
                        setupSeekbar(donationValue);

                    }

                }
            }
        });
        setupSeekbar(donationValue);
        seekBarMaint.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int progressleft = 100 - progress;
                if (fromUser) {
                    seekBarFamily.setProgress(progressleft);
                }
                correctText(progressleft,progress);


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        seekBarFamily.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int progressleft = 100 - progress;
                if (fromUser) {
                    seekBarMaint.setProgress(progressleft);
                }
                correctText(progress,progressleft);


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    private void correctText(int progressFamily, int progressDev){
        seekDevValue.setText(String.format("%.2f",(donationValue * progressDev/100.0))+" $");
        seekFamilyValue.setText(String.format("%.2f", (donationValue * progressFamily / 100.0))+" $");
    }

    public void onAcceptTerms(View view) {
        if(((CheckBox) view).isChecked()){
            checkout.setEnabled(true);
        }else{
            checkout.setEnabled(false);
        }

    }

    public void goBack(View view) {
        finish();
    }
}

package com.lendahand.util;

import android.media.MediaPlayer;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Bruno on 12/18/2015.
 */
public class AudioPlayer extends MediaPlayer {
    private String pathToPlay;

    public AudioPlayer(String pathToPlay){
        this.pathToPlay = pathToPlay;
    }

    public void play() {
        try{
            setDataSource(pathToPlay);
            prepare();
            start();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void stopPlay() {
        try {
            stop();
            release();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}

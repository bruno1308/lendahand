package com.lendahand.util;

import android.media.MediaRecorder;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import com.lendahand.user.User;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Bruno on 12/18/2015.
 */
public class AudioRecorder extends MediaRecorder {

    private String outputFile = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    public AudioRecorder(User userRecording){
        outputFile = Environment.getExternalStorageDirectory().
                getAbsolutePath() + "/"+userRecording.getFirstName()+userRecording.getLastName()+sdf.format(new Date())+".3gpp";

        setAudioSource(MediaRecorder.AudioSource.MIC);
        setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        setOutputFile(outputFile);
    }


    public void start(){
        try {
            prepare();
            start();
        } catch (IllegalStateException e) {
            // start:it is called before prepare()
            // prepare: it is called after start() or before setOutputFormat()
            e.printStackTrace();
        } catch (IOException e) {
            // prepare() fails
            e.printStackTrace();
        }
    }


    public void stop(){
        try {
            stop();
            release();
        } catch (IllegalStateException e) {
            //  it is called before start()
            e.printStackTrace();
        } catch (RuntimeException e) {
            // no valid audio/video data has been received
            e.printStackTrace();
        }
    }
}

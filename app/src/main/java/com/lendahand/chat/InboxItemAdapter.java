package com.lendahand.chat;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lendahand.R;

import java.util.ArrayList;

/**
 * Created by Bruno on 12/12/2015.
 */
public class InboxItemAdapter extends ArrayAdapter<InboxItem> {

    Context context;
    int layoutResourceId;
    ArrayList<InboxItem> data = null;

    public InboxItemAdapter(Context context, int layoutResourceId, ArrayList<InboxItem> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        InboxHolder holder = null;
        final InboxItem chat_being_rendered = data.get(position);

        if (row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(R.layout.inbox_single_item, parent, false); //Trocar por listview_item para visao com lista

            holder = new InboxHolder();
            holder.person_name = (TextView)row.findViewById(R.id.txtPersonName);
            holder.person_img = (ImageView)row.findViewById(R.id.imgPerson);
            holder.last_message = (TextView)row.findViewById(R.id.txtLastMsg);
            holder.time_ago = (TextView) row.findViewById(R.id.txtDate);

            row.setTag(holder);


        }
        else {
            holder = (InboxHolder)row.getTag();
        }




        //Save values into class
        holder.person_name.setText(chat_being_rendered.getPerson_name());
        holder.person_img.setImageResource(chat_being_rendered.getPerson_pic());
        holder.last_message.setText(chat_being_rendered.getLast_message());
        holder.time_ago.setText(chat_being_rendered.getDate());
        return row;
    }

    static class InboxHolder
    {
        public ImageView person_img;
        public TextView person_name;
        public TextView last_message;
        public TextView time_ago;


    }



}
package com.lendahand.chat;

/**
 * Created by Bruno on 12/12/2015.
 */
public class InboxItem {
    public int getPerson_pic() {
        return person_pic;
    }

    public void setPerson_pic(int person_pic) {
        this.person_pic = person_pic;
    }

    public String getPerson_name() {
        return person_name;
    }

    public void setPerson_name(String person_name) {
        this.person_name = person_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    private int person_pic;
    private String person_name;
    private String date;
    private String last_message;
}

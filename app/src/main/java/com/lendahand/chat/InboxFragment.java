package com.lendahand.chat;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lendahand.R;

import java.util.ArrayList;

public class InboxFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private ArrayList<InboxItem> inboxes;
    private static Context context;
    public InboxFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static InboxFragment newInstance(Context c) {
        InboxFragment fragment = new InboxFragment();
        context = c;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_inbox, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.listview_inboxes);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
               handleClick(arg0,arg1,arg2,arg3);
            }

        });

        InboxItemAdapter adapter = new InboxItemAdapter(getContext(), R.id.listview_inboxes, loadInboxes());
        listView.setAdapter(adapter);

        return rootView;
    }

    public ArrayList<InboxItem> loadInboxes(){
        inboxes = new ArrayList<>();
        InboxItem item1 = new InboxItem();
        item1.setDate("20 Nov 2015");
        item1.setPerson_name("Fúlvio Abrahão");
        item1.setPerson_pic(R.drawable.fb_fulvio);
        item1.setLast_message("Hey Fúlvio, let's help some folks???");

        InboxItem item2 = new InboxItem();
        item2.setDate("20 Oct 2015");
        item2.setPerson_name("João Jota");
        item2.setPerson_pic(R.drawable.fb_jota);
        item2.setLast_message("What's up Jota? What are you doing now?");

        inboxes.add(item1);
        inboxes.add(item2);
        return inboxes;

    }

    public void handleClick(AdapterView<?> arg0, View arg1, int position, long id){
        Intent openConversation = new Intent(getContext(), ConversationActivity.class);
        InboxItem inboxItem = (InboxItem) arg0.getItemAtPosition(position);
        openConversation.putExtra("NAME",inboxItem.getPerson_name());
        startActivity(openConversation);


    }
}

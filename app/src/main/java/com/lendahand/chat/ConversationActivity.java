package com.lendahand.chat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.lendahand.R;
import com.lendahand.chat.ConversationAdapter;
import com.lendahand.chat.Message;

public class ConversationActivity extends AppCompatActivity {

    private ListView listViewMessages;
    private EditText sendMessage;
    private ConversationAdapter adapter;
    private String personName;
    private ImageView imgBack;
    private ImageView imgPerson;
    private TextView txtPersonName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        listViewMessages = (ListView) findViewById(R.id.listView1);

        adapter = new ConversationAdapter(getApplicationContext(), R.layout.message_single_item);

        listViewMessages.setAdapter(adapter);

        sendMessage = (EditText) findViewById(R.id.editText1);
        sendMessage.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    adapter.add(new Message(false, sendMessage.getText().toString()));
                    sendMessage.setText("");
                    return true;
                }
                return false;
            }
        });
        personName = getIntent().getStringExtra("NAME");
        imgBack = (ImageView)findViewById(R.id.imgBack);
        txtPersonName = (TextView)findViewById(R.id.txtPersonNameConversation);
        imgPerson = (ImageView)findViewById(R.id.imgPersonConversation);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });
        addItems();


    }

    private void goBack() {
        finish();
    }

    private void addItems() {
        txtPersonName.setText(personName);
        if(personName.equalsIgnoreCase("Fúlvio Abrahão")) {
            adapter.add(new Message(false, "Hello buddy!"));
            adapter.add(new Message(true, "Hey, what's up?"));
            adapter.add(new Message(false, "Hey Fúlvio, let's help some folks???"));
        }else if(personName.equalsIgnoreCase("João Jota")) {
            adapter.add(new Message(false, "What's up Jota? What are you doing now?"));
            imgPerson.setImageResource(R.drawable.fb_jota);

        }


    }
}

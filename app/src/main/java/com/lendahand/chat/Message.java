package com.lendahand.chat;

/**
 * Created by Bruno on 12/12/2015.
 */
public class Message {
    public boolean left;
    public String comment;

    public Message(boolean left, String comment) {
        super();
        this.left = left;
        this.comment = comment;
    }

}

package com.lendahand;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity {
    EditText txtFirstName;
    EditText txtLastName;
    EditText txtEmail;
    EditText txtPassword;
    ImageView imgFirstNameCheck;
    ImageView imgLastNameCheck;
    ImageView imgEmailCheck;
    ImageView imgPasswordCheck;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setupToolbar();
        attachUI();
        realTimeCheck();

    }

    private void attachUI() {
        txtFirstName = (EditText)findViewById(R.id.txtFirstName);
        txtLastName = (EditText)findViewById(R.id.txtLastName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        txtPassword = (EditText)findViewById(R.id.txtPassword);
        imgFirstNameCheck = (ImageView)findViewById(R.id.imgFirstNameCheck);
        imgLastNameCheck = (ImageView)findViewById(R.id.imgLastNameCheck);
        imgEmailCheck = (ImageView)findViewById(R.id.imgEmailCheck);
        imgPasswordCheck = (ImageView)findViewById(R.id.imgPasswordCheck);
    }

    public void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSignUp);
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.menu_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Create new account");
    }

    public void createNewAccount(View view) {
        String firstName,lastName,password,email;
        firstName = txtFirstName.getText().toString();
        lastName = txtLastName.getText().toString();
        email = txtEmail.getText().toString();
        password = txtPassword.getText().toString();
        view.requestFocus();
        view.requestFocusFromTouch();
        if(validatePassword(password)){
            loginMain();

        }
    }
    public boolean validatePassword(String password){
        if(password.length() <6){
            return false;
        }
        return true;
    }
    public void realTimeCheck(){


        txtPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                    if (event.getRawX() >= (txtPassword.getRight() - txtPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            txtPassword.setTransformationMethod(null);
                        }else if(event.getAction() == MotionEvent.ACTION_UP){
                            txtPassword.setTransformationMethod(new PasswordTransformationMethod());
                            //Estranhamente, o cursor vai pro começo quando solto o touch, então,
                            //Volte-o para o final
                            txtPassword.setSelection(txtPassword.length());
                        }

                        return true;
                    }

                return false;
            }
        });


        txtFirstName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    imgFirstNameCheck.setVisibility(View.VISIBLE);
                    if(txtFirstName.length()>0){
                        imgFirstNameCheck.setImageResource(R.drawable.ok);
                    }else{
                        imgFirstNameCheck.setImageResource(R.drawable.error);
                    }
                }
            }
        });

        txtLastName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    imgLastNameCheck.setVisibility(View.VISIBLE);
                    if(txtLastName.length()>0){
                        imgLastNameCheck.setImageResource(R.drawable.ok);
                    }else{
                        imgLastNameCheck.setImageResource(R.drawable.error);
                    }
                }
            }
        });

        txtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    imgEmailCheck.setVisibility(View.VISIBLE);
                    if(txtEmail.length()>5){
                        imgEmailCheck.setImageResource(R.drawable.ok);
                    }else{
                        imgEmailCheck.setImageResource(R.drawable.error);
                    }
                }
            }
        });

        txtPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    imgPasswordCheck.setVisibility(View.VISIBLE);
                    if(validatePassword(txtPassword.getText().toString())){
                        imgPasswordCheck.setImageResource(R.drawable.ok);
                    }else{
                        imgPasswordCheck.setImageResource(R.drawable.error);
                    }
                }
            }
        });

    }
    public void loginMain(){
        Intent i = new Intent(SignUpActivity.this,MainActivity.class);
        startActivity(i);

    }

    public void revealPassword(){

    }
}

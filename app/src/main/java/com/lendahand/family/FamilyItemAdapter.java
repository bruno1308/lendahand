package com.lendahand.family;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lendahand.R;
import com.lendahand.timeline.TimelineItem;

import java.util.ArrayList;

/**
 * Created by Bruno on 12/13/2015.
 */

public class FamilyItemAdapter extends ArrayAdapter<Family> {

    static Context context;
    int layoutResourceId;
    ArrayList<Family> data = null;

    public FamilyItemAdapter(Context context, int layoutResourceId, ArrayList<Family> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        FamilyHolder holder = null;
        final Family item_being_rendered = data.get(position);

        if (row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(R.layout.family_single_item, parent, false); //Trocar por listview_item para visao com lista

            holder = new FamilyHolder();
            holder.angel_name = (TextView)row.findViewById(R.id.txtAngelname);
            holder.angel_pic = (ImageView)row.findViewById(R.id.imgAngelPic);
            holder.post_img = (ImageView)row.findViewById(R.id.imgPostPicFamily);
            holder.post_text = (TextView)row.findViewById(R.id.txtPostTextFamily);
            holder.time_ago = (TextView) row.findViewById(R.id.txtTimeAgoFamily);
            holder.city = (TextView)row.findViewById(R.id.txtCity);
            holder.money = (TextView)row.findViewById(R.id.txtMoney);
            holder.num_people = (TextView)row.findViewById(R.id.txtNumPeople);
            holder.country_flag = (ImageView)row.findViewById(R.id.imgCountryFlag);



            row.setTag(holder);


        }
        else {
            holder = (FamilyHolder)row.getTag();
        }


        //Save values into class
        holder.angel_name.setText(item_being_rendered.getAngel_name());
        holder.angel_pic.setImageResource(item_being_rendered.getAngel_pic());
        holder.post_img.setImageResource(item_being_rendered.getPicture_resid());
        holder.post_text.setText(item_being_rendered.getText_post());
        holder.time_ago.setText(item_being_rendered.getDate_post());
        holder.city.setText(item_being_rendered.getCity_name());
        holder.money.setText(Double.toString(item_being_rendered.getMoney_needed())+"$");
        holder.num_people.setText(Integer.toString(item_being_rendered.getNum_people()));
        holder.country_flag.setImageResource(item_being_rendered.getCountry_flag_resid());

        return row;
    }

    static class FamilyHolder
    {
        public ImageView angel_pic;
        public TextView angel_name;
        public ImageView post_img;
        public TextView post_text;
        public TextView time_ago;
        public TextView money;
        public TextView num_people;
        public TextView city;
        public ImageView country_flag;

    }




}
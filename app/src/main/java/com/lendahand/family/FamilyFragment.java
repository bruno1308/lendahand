package com.lendahand.family;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.lendahand.CheckoutActivity;
import com.lendahand.R;

import java.util.ArrayList;

/**
 * Created by Bruno on 12/12/2015.
 */
public class FamilyFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static Context context;

    public FamilyFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static FamilyFragment newInstance(Context c) {
        context = c;
        FamilyFragment fragment = new FamilyFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_family, container, false);
        ListView listViewFamilies = (ListView) rootView.findViewById(R.id.listview_families);

        FamilyItemAdapter familyItemAdapter = new FamilyItemAdapter(context,R.layout.fragment_family,getFamilies());
        listViewFamilies.setAdapter(familyItemAdapter);
        return rootView;
    }



    private ArrayList<Family> getFamilies(){
        ArrayList<Family> families = new ArrayList<>();
        Family f1 = new Family();
        Family f2 = new Family();

        f1.setAngel_name("Emílio Fernandes");
        f1.setAngel_pic(R.drawable.emilio);
        f1.setCity_name("São Bento do Sapucaí - São Paulo");
        f1.setCountry_flag_resid(R.drawable.brazil);
        f1.setFamily_name("");
        f1.setDate_post("20 min ago");
        f1.setNum_people(8);
        f1.setMoney_needed(900.0);
        f1.setPicture_resid(R.drawable.family_help1);
        f1.setText_post("People, this family has 8 members that scavenge for food in trash cans everyday, if someone could help them, we would appreciate it. Thank you!! ");

        f2.setAngel_name("Isabela Fernandes");
        f2.setAngel_pic(R.drawable.isabela);
        f2.setCity_name("Santiago");
        f2.setCountry_flag_resid(R.drawable.chile);
        f2.setFamily_name("");
        f2.setDate_post("1 hour ago");
        f2.setNum_people(1);
        f2.setMoney_needed(200.0);
        f2.setPicture_resid(R.drawable.family_help2);
        f2.setText_post("This guy goes from 8am to 10pm collecting cardboards all across the city, so he can sell it and make his living." +
                " We're aiming to help him go through this month without having to work that much. Thank you all! ");

        families.add(f1);
        families.add(f2);
        return families;
    }
}

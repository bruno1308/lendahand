package com.lendahand.family;

/**
 * Created by Bruno on 12/13/2015.
 */
public class Family {
    private String family_name;
    private int num_people;
    private double money_needed;
    private String angel_name;

    public int getAngel_pic() {
        return angel_pic;
    }

    public void setAngel_pic(int angel_pic) {
        this.angel_pic = angel_pic;
    }

    private int angel_pic;
    private String city_name;

    public int getPicture_resid() {
        return picture_resid;
    }

    public void setPicture_resid(int picture_resid) {
        this.picture_resid = picture_resid;
    }

    public int getCountry_flag_resid() {
        return country_flag_resid;
    }

    public void setCountry_flag_resid(int country_flag_resid) {
        this.country_flag_resid = country_flag_resid;
    }

    public String getText_post() {
        return text_post;
    }

    public void setText_post(String text_post) {
        this.text_post = text_post;
    }

    public String getDate_post() {
        return date_post;
    }

    public void setDate_post(String date_post) {
        this.date_post = date_post;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getAngel_name() {
        return angel_name;
    }

    public void setAngel_name(String angel_name) {
        this.angel_name = angel_name;
    }

    public double getMoney_needed() {
        return money_needed;
    }

    public void setMoney_needed(double money_needed) {
        this.money_needed = money_needed;
    }

    public int getNum_people() {
        return num_people;
    }

    public void setNum_people(int num_people) {
        this.num_people = num_people;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    private String date_post;
    private String text_post;
    private int country_flag_resid;
    private int picture_resid;
}
